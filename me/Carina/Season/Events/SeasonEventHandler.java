package me.Carina.Season.Events;

import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.weather.ThunderChangeEvent;
import org.bukkit.event.weather.WeatherChangeEvent;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;

import me.Carina.Season.Main;
import me.Carina.Season.Season;

public class SeasonEventHandler implements Listener
{
	static Main plugin;
	static Season season;
	public SeasonEventHandler (Main m)
	{
		plugin = m;
		season = m.getSeason();
	}
	
	@EventHandler(priority = EventPriority.LOWEST)
	public void onJoin(PlayerJoinEvent event)
	{
		Player p = event.getPlayer();
		if(p.hasPlayedBefore())
		{
			// 
		}
	}
	
	@EventHandler(priority = EventPriority.LOWEST)
	public void onQuit(PlayerQuitEvent event)
	{
		Player p = event.getPlayer();
		for(Player target : Bukkit.getOnlinePlayers())
		{
			//
		}
	}
	
	@EventHandler(priority = EventPriority.LOWEST)
	public void onWeatherChange(WeatherChangeEvent event)
	{
		if(event.toWeatherState())
		{
			for(Player target : Bukkit.getOnlinePlayers())
			{
				target.sendMessage(ChatColor.AQUA + " It's raining !");
				if(season.WeatherChangeAllowed())
				{
					season.SetAllowWeatherChange(false);
				}
				else
				{
					event.setCancelled(true);
					target.sendMessage(ChatColor.AQUA + "Ah... Finally no.");
				}
			}
		}
	}
	
	@EventHandler(priority = EventPriority.LOWEST)
	public void onThunderChange(ThunderChangeEvent event)
	{
		for(Player target : Bukkit.getOnlinePlayers())
		{
			target.sendMessage(ChatColor.AQUA + "Pikachu, thunder !");
			if(season.WeatherChangeAllowed())
			{
				season.SetAllowWeatherChange(false);
			}
			else
			{
				event.setCancelled(true);
				target.sendMessage(ChatColor.AQUA + "Ah... Finally no.");
			}
		}
	}
}
