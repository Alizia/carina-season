package me.Carina.Season;

import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import me.Carina.Season.Commands.SeasonCommands;
import me.Carina.Season.Events.SeasonEventHandler;

public class Main extends JavaPlugin 
{
	Season season;
	
	@Override
	public void onEnable()
	{
		this.saveDefaultConfig();
		this.getConfig().set("bite-size", 25.0);
		//this.reloadConfig();
		//this.saveConfig();
		
		registerCmds();
		
		season = new Season(this);
		
		PluginManager pm = this.getServer().getPluginManager();
		pm.registerEvents(new SeasonEventHandler(this), this);
	}
	
	@Override
	public void onDisable()
	{
		
	}
	
	public void registerCmds()
	{
		this.getCommand("season").setExecutor(new SeasonCommands(this));
		this.getCommand("saison").setExecutor(new SeasonCommands(this));
	}
	
	public Season getSeason()
	{
		return season;
	}
}