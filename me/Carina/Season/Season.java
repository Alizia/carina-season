package me.Carina.Season;

import java.util.Calendar;
import java.util.List;

public class Season
{
	static Main plugin;
	Calendar configCalendar;
	Calendar spring;
	Calendar summer;
	Calendar autumn;
	Calendar winter;
	long ratio;
	
	boolean allowWeatherChange;
	
	public Season(Main m)
	{
		plugin = m;
		
		configCalendar = Calendar.getInstance();
		
		// Season start date -1
		spring = Calendar.getInstance();
		spring.set(spring.get(Calendar.YEAR), 3, 19); // Year, month, date...
		
		summer = Calendar.getInstance();
		summer.set(summer.get(Calendar.YEAR), 6, 19); // Year, month, date...
		
		autumn = Calendar.getInstance();
		autumn.set(autumn.get(Calendar.YEAR), 9, 22); // Year, month, date...
		
		winter = Calendar.getInstance();
		winter.set(winter.get(Calendar.YEAR), 12, 21); // Year, month, date...		
		
		// Init config start date
		List<Integer> configDate = plugin.getConfig().getIntegerList("start-date");
		configCalendar.set(configDate.get(2), configDate.get(1)-1, configDate.get(0));
		
		ratio = plugin.getConfig().getLong("ratio-day-year");
		
		allowWeatherChange = false;
	}
	
	public Calendar GetCalendarByName(String calendarName)
	{
		switch(calendarName)
		{
			case "configCalendar":
				return configCalendar;
			case "spring":
				return spring;
			case "summer":
				return summer;
			case "autumn":
				return autumn;
			case "winter":
				return winter;
			default:
				return null;
		}
	}
	
	public long GetRatio()
	{
		return ratio;
	}
	
	public boolean WeatherChangeAllowed()
	{
		return allowWeatherChange;
	}
	
	public void SetAllowWeatherChange(boolean mode)
	{
		allowWeatherChange = mode;
	}
}
