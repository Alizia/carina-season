package me.Carina.Season.Commands;

import java.util.Calendar;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.Carina.Season.Main;
//import me.Carina.Season.Season;

public class SeasonCommands implements CommandExecutor
{
	static Main plugin;
	public SeasonCommands(Main m)
	{
		plugin = m;
	}
	
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
	{
		if(label.equalsIgnoreCase("season") || label.equalsIgnoreCase("saison"))
		{
			if(args.length > 0) 
			{
				for(Player target : Bukkit.getOnlinePlayers())
				{
					target.sendMessage(ChatColor.AQUA + "args[0] = " + args[0]);
				}
				
				switch(args[0])
				{
					case "rain":
						plugin.getSeason().SetAllowWeatherChange(true);
						Bukkit.getWorlds().get(0).setStorm(true);
						Bukkit.getWorlds().get(0).setWeatherDuration(5*60*20); // 5 minutes, 20 tick/s
						Bukkit.getWorlds().get(0).setThunderDuration(5*60*20);
						
						for(Player target : Bukkit.getOnlinePlayers())
						{
							target.sendMessage(ChatColor.AQUA + "getWeatherDuration() = " + plugin.getServer().getWorlds().get(0).getWeatherDuration());
							target.sendMessage(ChatColor.AQUA + "hasStorm()" + plugin.getServer().getWorlds().get(0).hasStorm());
						}
						
						return true;
					case "thunder":
						plugin.getSeason().SetAllowWeatherChange(true);
						Bukkit.getWorlds().get(0).setThunderDuration(5*60*20); // 5 minutes, 20 tick/s
						Bukkit.getWorlds().get(0).setThundering(true);
						return true;
					default:
						return false;
				}
			}
			else
			{
				Calendar now = Calendar.getInstance();
				
				for(Player target : Bukkit.getOnlinePlayers())
				{
					//target.sendMessage(ChatColor.AQUA + "configCalendar = " + GetStringDate(configCalendar));
					//target.sendMessage(ChatColor.AQUA + "now = " + GetStringDate(now));
					//target.sendMessage(ChatColor.AQUA + "ratio = " + ratio);
					
					//Calendar test = Calendar.getInstance();
					//test.setTimeInMillis(now.getTimeInMillis() - configCalendar.getTimeInMillis());
					//target.sendMessage(ChatColor.AQUA + "diff = " + GetStringDate(test));
				}
				
				
				now.setTimeInMillis(plugin.getSeason().GetCalendarByName("configCalendar").getTimeInMillis() + (now.getTimeInMillis() - plugin.getSeason().GetCalendarByName("configCalendar").getTimeInMillis()) * plugin.getSeason().GetRatio()); // Apply ratio Year/Day
							
				String s = "Winter is coming !";
				
				if(now.after(plugin.getSeason().GetCalendarByName("spring")))
				{
					s = "Spring !";
				}
				if(now.after(plugin.getSeason().GetCalendarByName("summer")))
				{
					s = "Summer !";
				}
				if(now.after(plugin.getSeason().GetCalendarByName("autumn")))
				{
					s = "Automn !";
				}
				if(now.after(plugin.getSeason().GetCalendarByName("winter")))
				{
					s = "Winter is coming !";
				}
							
				for(Player target : Bukkit.getOnlinePlayers())
				{
					target.sendMessage(ChatColor.AQUA + s + "Now = " + GetStringDate(now));
				}
				
				return true;
			}
			
		}
		
		return false;
	}
	
	public String GetStringDate(Calendar c)
	{
		String s = "%day% / %month% / %year%";
				
		s = s.replace("%day%", Integer.toString(c.get(Calendar.DAY_OF_MONTH)));
		s = s.replace("%month%", Integer.toString(c.get(Calendar.MONTH)+1)); 
		s = s.replace("%year%", Integer.toString(c.get(Calendar.YEAR)));
		
		return s;
	}
}
